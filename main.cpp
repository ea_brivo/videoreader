#include <stdio.h>
#include <stdlib.h>
#include <chrono>
#include <ctime>
#include <unistd.h>
#include <iostream>
#include <wiringPi.h>

#include "lib.h"

using namespace std;

void setup(void);
void loop(void);
int vid_length;

int main(){
    time_t now;                                 // initialize time variable
    char buf[sizeof"YYYY-MM-DDThh:mm:ssZ"];     // initialize timestamp buffer
    vid_length = 10;                        // total buffer length in seconds
    int vid_offset = 5;                         // record time before event
    
    setup(); // run setup

    bool loop = true; // set var for holding loop

    // loop until event is triggered
    while(loop){
        if(digitalRead(0) == 1){ // Trigger with GPIO
            printf("Event Triggered\n");std::cout.flush(); // Log message
            time(&now); // refresh time
            strftime(buf, sizeof buf, "%FT%TZ", gmtime(&now)); // format time
            loop = false; // exit loop
        }

        delay(1000 / 20); // check for event only 20x per second
    }

    string str(buf); // timestamp buffer to string
    string cmd_s = "cp event.h264 " + str + ".h264"; // form string for timestamped file
    char* cmd;
    cmd = &cmd_s[0]; // convert string back to char array

    sleep(vid_length - vid_offset); // wait for the offset time before saving clip

    system("killall -s SIGUSR1 raspivid");  // send signal to raspivid to save buffer
    system(cmd);                            // copy raw event video to timestamped backup
    printf("Buffer Saved\n");std::cout.flush(); // log message
    
    system("ffmpeg -r 40 -i event.h264 -vcodec copy -y -v 24 event.mp4"); // re-encode video
    printf("Encoding Complete\n");std::cout.flush(); // log message
}

// setup and prepare video buffer
void setup(){
    wiringPiSetup(); // start GPIO library with default pin counts
    pinMode(0, INPUT); // set pin to input
    pullUpDnControl(0, PUD_DOWN); // set pin to use internal pulldown
    printf("GPIO Started\n");std::cout.flush(); // log message

    // Video buffer command
    // rotation is 180 for stock sensor, 270 for wide angle sensor
    string cmd_s = "raspivid -s -md 4 -fps 40 -lev 4.2 -pf high -g 5 -rot 270 -c -t " + to_string(vid_length) + "000 -n -o event.h264 &";
    char* cmd = &cmd_s[0];

    system(cmd); // start video buffer
    printf("Recording Started\n");std::cout.flush(); // log message
}